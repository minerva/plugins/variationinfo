require('../css/styles.css');
require('bootstrap-treeview');
require('bootstrap-table');

const linkify = require("html-linkify");



const pluginName = 'variationinfo';
const pluginVersion = '1.0.0';

const globals = {
    selected: [],
    allBioEntities: [],
    pickedRandomly: undefined,
    goupedData: undefined,
    expanded: false,
    gene2BioEntities: {}
};

// ******************************************************************************
// ********************* PLUGIN REGISTRATION WITH MINERVA *********************
// ******************************************************************************

let minervaProxy;
let pluginContainer;
let pluginContainerId;


const register = function(_minerva) {

    // console.log('registering ' + pluginName + ' plugin');

    $(".tab-content").css('position', 'relative');

    minervaProxy = _minerva;
    pluginContainer = $(minervaProxy.element);
    pluginContainerId = pluginContainer.attr('id');

    // console.log('minerva object ', minervaProxy);
    // console.log('project id: ', minervaProxy.project.data.getProjectId());
    // console.log('model id: ', minervaProxy.project.data.getModels()[0].modelId);

    initPlugin();
};

const unregister = function () {
    // console.log('unregistering ' + pluginName + ' plugin');

    unregisterListeners();
    return deHighlightAll();
};

const getName = function() {
    return pluginName;
};

const getVersion = function() {
    return pluginVersion;
};

/**
 * Function provided by Minerva to register the plugin
 */
minervaDefine(function (){
    return {
        register: register,
        unregister: unregister,
        getName: getName,
        getVersion: getVersion
        ,minWidth: 300
        ,defaultWidth: 600
    }
});

function initPlugin () {
    registerListeners();
    const container = initMainContainer();
    initGene2BioEntities().then(function(){
        container.find('.genes-loading').hide();
        initMainPageStructure(container);
    });


}

function registerListeners(){
    // minervaProxy.project.map.addListener({
    //     dbOverlayName: "search",
    //     type: "onSearch",
    //     callback: searchListener
    // });
}

function unregisterListeners() {
    // minervaProxy.project.map.removeAllListeners();
}

// ****************************************************************************
// ********************* MINERVA INTERACTION*********************
// ****************************************************************************


function deHighlightAll(){
    return minervaProxy.project.map.getHighlightedBioEntities().then( highlighted => minervaProxy.project.map.hideBioEntity(highlighted) );
}

// ****************************************************************************
// ********************* PLUGIN STRUCTURE AND INTERACTION*********************
// ****************************************************************************

function getContainerClass() {
    return pluginName + '-container';
}

function initMainContainer(){
    const container = $(`<div class="${getContainerClass()}"></div>`).appendTo(pluginContainer);
    container.append(`
    <div class="panel panel-default genes-loading">
        <div class="panel-body">  
            <i class="fa fa-circle-o-notch fa-spin"></i> Obtaining gene-bioentities mapping from the map
        </div>        
    </div>`);

    return container;
}

function initMainPageStructure(container){

    container.append(`
        <form class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label">Disease: </label>
                <div class="col-sm-9">
                    <input class="form-control-static input-disease" type="text" value="parkinson">
                </div>
            </div>                                        
        </form>
        <button type="button" class="btn-search btn btn-primary btn-default btn-block">${searching(false)}</button>
        
        <div class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Error</h4>
                    </div>
                    <div class="modal-body">
                        <p class="text-warning">Please fill in a disease for which variations should be located.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div> 
    `);

    container.find('.btn-search').on('click', () => search() );
    container.find('.input-disease').keypress(e => {if (e.which == 13) {search(); return false}} );
}

function recreateBootstrapTable(){
    pluginContainer.find('.bootstrap-table').remove();
    $('<table class="variation-table" data-detail-view="true"></table>').appendTo(pluginContainer.find(`.${getContainerClass()}`));

}

function initGene2BioEntities(){
    return minervaProxy.project.data.getAllBioEntities().then(function(bes){
        let aliases = bes.filter(be => be.constructor.name === 'Alias');
        aliases.forEach(a => {
            let ref = a.getReferences().filter(r=> r.getType() === 'HGNC_SYMBOL');
            if (ref.length > 0) {
                let hgncSymbol = ref[0].getResource();
                if (!(hgncSymbol in globals.gene2BioEntities)) globals.gene2BioEntities[hgncSymbol] = [];
                globals.gene2BioEntities[hgncSymbol].push({
                    id: a.getId(),
                    modelId: a.getModelId()
                });
            }
        });
        // console.log('globals.gene2BioEntities', globals.gene2BioEntities);
    });
}

function searchListener(entities) {
    if (entities[0].length > 0 && entities[0][0].constructor.name === 'Alias') {
        globals.selected = entities[0];

        pluginContainer.find('.input-start').html(globals.selected[0].getName());
    }
}

function queryMinervaApi(query, parameters, queryType) {

    const apiAddress = ServerConnector.getApiBaseUrl();
    if (!queryType) queryType = 'GET';
    if (!parameters) parameters = '';

    return $.ajax({
        type: queryType,
        url: `${apiAddress}/${query}`,
        // dataType: 'json',
        data: parameters
    })
}

function queryUniprot(disease) {

    return $.ajax({
        url: `https://www.ebi.ac.uk/proteins/api/variation?offset=0&size=-1&disease=${disease}`
    })
}

function isString(s) {
    return typeof(s) === 'string' || s instanceof String;
}

function getVariantsTree(variants) {

    function getLevel(obj, name) {

        if (obj instanceof Array) {
            return obj.map((o,i) => {
                return {
                    text: `${name}  ${i+1}`,
                    nodes: getLevel(o)
                }
            })
        }

        const l = [];
        let i = 0;
        for (let k in obj) {
            if (obj[k] instanceof Array || obj[k] instanceof Object) {
                l.push({
                    text: k,
                    nodes: getLevel(obj[k], k.replace(/s *$/, '')),
                    state: {expanded: globals.expanded}
                });

            } else {
                let linkified = isString(obj[k]) ? linkify(obj[k], {attributes: {target:'_blank'}}) : obj[k];
                l.push({
                    text: `${k}: ${linkified}`,
                    selectable: false
                });
            }

        }

        return l;
    }

    return variants.map(v => { return {
        text: v.entryName,
        nodes: getLevel(v),
        state: {expanded: globals.expanded}
    };});

}

function getKeyFromVariant(v) {
    return `${v.geneName}-${v.organismName}`;
}

function groupVariants(data) {
    const res = {};

    for (let i in data) {
        const d = data[i];
        const key = getKeyFromVariant(d);
        if (!(key in res)) res[key] = [];
        res[key].push(d);
    }

    return res;
}

function searching(inProgress) {
    return inProgress ?
        '<i class="fa fa-circle-o-notch fa-spin"></i> Retrieving' :
        'Retrieve';
}

function activateSearchBtton(activate) {
    if (activate) {
        deHighlightAll();
        recreateBootstrapTable();
    }
    pluginContainer.find('.btn-search').html(searching(activate));
}

function showModal() {
    pluginContainer.find(".modal").modal('show');
}

const geneFormatter = function (geneName) {
    let icon = geneName in globals.gene2BioEntities ?
        `<i class="glyphicon glyphicon-eye-open" data-gene="${geneName}"></i>` :
        '';

    return `${icon} ${geneName}`;
};

function search() {

    const disease = pluginContainer.find('.input-disease').val();

    if (!disease) {
        showModal();
        return;
    }

    activateSearchBtton(true);

    // const projectId = minervaProxy.project.data.getProjectId();
    // const modelId = globals.selected[0].getModelId();

    // console.log('disease', disease);
    queryUniprot(disease).then( data => {
        console.log("data", data);
        globals.groupedData = groupVariants(data);
        console.log(globals.groupedData);
        let varTable = pluginContainer.find('.variation-table');
        varTable.bootstrapTable({
            columns: [{
                field: 'geneName',
                title: 'Gene',
                sortable: true,
                formatter: geneFormatter,
                searchable: true
            },{
                field: 'occurrencesCnt',
                title: 'Occurrences',
                sortable: true
            },{
                field: 'isoformsCnt',
                title: 'Isoforms',
                sortable: true
            }, {
                field: 'organismName',
                title: 'Organism',
                sortable: true
            }],
            data: Object.values(globals.groupedData).map(d => {return {
                    geneName:d[0].geneName,
                    organismName:d[0].organismName,
                    occurrencesCnt: d[0].geneName in globals.gene2BioEntities ? globals.gene2BioEntities[d[0].geneName].length : 0,
                    isoformsCnt: d.length
                }; }
            ),
            sortName: 'occurrencesCnt',
            sortOrder: 'desc',
            onExpandRow: function (index, row, $detail) {
                let key = getKeyFromVariant(row);
                const tree = $(`<div id="tree-${key}"></div>`);
                tree.treeview({
                    data: getVariantsTree(globals.groupedData[key]),
                    enableLinks: true
                });
                // tree.unbind("click"); //default
                $detail.append(tree);
            }
        });

        varTable.on('post-body.bs.table', function() {
            // sorting recreates objects, thus removing events
            // instead of using sort.bs.table or onSort, we need to used post-body, because sort.bs.table
            // is called before the actual sorting
            registerTableEvents();
        });
        setTableHeight(varTable);
        activateSearchBtton(false);

        registerTableEvents();
    });

    // getNeighbors([startBiotEntity], direction, cntSteps-1, modsBlock,  [startBiotEntity]).then(function (data) {
    //     console.log("getNeighbors data", data);
    //     let hull = getConcaveHull(data.species, data.reactions);
    //     console.log("hull", hull);
    //     const queryParams = {
    //         handlerClass: handlerClass,
    //         polygonString: getPolygonString(hull)
    //     };
    //
    //     queryApi(`projects/${projectId}/models/${modelId}:downloadModel`, queryParams).then((model) => {
    //         console.log("model", model);
    //         let model_filtered = filterModel(model, data.species, data.reactions);
    //         console.log("model_filtered", model_filtered);
    //         const fileName = `export_${startName}_${Directions.properties.labels[direction]}_steps-${cntSteps}_modblock-${modsBlock}.${fileSuffix}`.toLowerCase();
    //         storeToFile(fileName, model_filtered);
    //         activateExportBtton(false);
    //     }, (error) => {
    //         console.log("error", error);
    //         activateExportBtton(false);
    //     });
    // });
}

function setTableHeight($table) {
    let $ftb = $table.closest('.fixed-table-body');
    let top = $ftb.offset().top;
    $ftb.css('height', `calc(100vh - ${top}px - 20px)`);
}

function highlightGeneOccurrences(geneName){
    return deHighlightAll().then(function () {
        let bioEntities = globals.gene2BioEntities[geneName];
        let highlightDefs = bioEntities.map(e => {
            return {
                element: {
                    id: e.id,
                    modelId: e.modelId,
                    type: "ALIAS"
                },
                type: "ICON"
            };
        });
        return minervaProxy.project.map.showBioEntity(highlightDefs);
    });
}

function registerTableEvents(){
    pluginContainer.find('.variation-table .glyphicon-eye-open').each((i, e) =>{
        e.onclick = function () {
            highlightGeneOccurrences(e.dataset.gene);
        };
    });
}