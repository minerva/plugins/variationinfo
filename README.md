# !!!! Deprecated. Use the disease associations plugin instead !!!

# Variation exploration

`Variationinfo` is a plugin allowing to see variation information available in public resources in the context
of current disease map. 

### General instructions

In order to use the precompiled and publicly available version of the plugin, 
open the plugin menu in the MINERVA's upper left corner (see image below) and click plugins.
In the dialog which appears enter the following address in the URL box: `https://minerva-dev.lcsb.uni.lu/plugins/variationinfo/plugin.js` .
The plugin shows up in the plugins panel on the right hand side of the screen.

### Plugin functionality

- The users inputs disease of interest as a plain string
- The plugin connects to the [Proteins API variation service](https://www.ebi.ac.uk/proteins/api/doc/#!/variation/) 
and retrieves all available variants for that disease.
- Returned variants are already grouped by isoforms of genes they are involved in. 
The plugin groups variants in isoforms by gene name
and displays them in a table where each row contains the gene name, number of occurrences of that gene in
the map and number of isoforms obtained from the Protein API.
- The user can click on the eye icon of a particular gene to show its occurrences in the map. If there is no occurrence
of given gene in the map, the number of occurrences is zero and no eye icon is displayed.
- By clicking on the plus symbol in each line line, one can explore all the data which was obtained from 
the Protein API for that gene. 

### Data resources

The information about the variants comes from EBI's [Proteins API](https://www.ebi.ac.uk/proteins/api/doc/#!/variation/search).
As stated on the pages of Proteins API *"The variation, proteomics and antigen services 
provide annotations imported and mapped from large scale data sources, 
such as 1000 Genomes, ExAC (Exome Aggregation Consortium), 
TCGA (The Cancer Genome Atlas), COSMIC (Catalogue Of Somatic Mutations In Cancer), 
PeptideAtlas, MaxQB (MaxQuant DataBase), EPD (Encyclopedia of Proteome Dynamics) and HPA, 
along with UniProtKB annotations for these feature types (if applicable)"*. 

The following image shows an example of the data (a snippet from Google Chrome DevTools) 
already grouped by gene name and species information (see above).

![Grouped Proteins API data](img/data_example.png)


### Known issues

- The disease name can be entered only as a general string and not as an ontology term. Therefore, queries such as 
"parkinson" and "parkinson's" will return two distinct sets of variants.